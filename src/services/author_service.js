import api from "../api/api"
const author="author"
export const fetchAuthor = async () => {
    let response = await api.get(author)
    return response.data.data
}
export const CreateAuthor = async (category) => {
    let response = await api.post(author, category)
    return response.data.message
}
export const deleteAuthor = async (id) => {
    let response = await api.delete(author+"/" + id)
    return response.data.message
}
export const updateAuthorById = async (id, newAuthor) => {
    let {name,email,image}=newAuthor
    let authorMapped={
        name,
        email,
        image
    }
    let response = await api.put(author+"/" + id,authorMapped)
    return response.data.message
}
export const uploadImage = async (file) => {
    let formData = new FormData()
    formData.append('image', file)
    let response = await api.post('images', formData)
    return response.data.url
}