import React, { useEffect, useState } from "react";
import { Container, Form, Table, Button, Row, Col } from "react-bootstrap";
import {
  fetchAuthor,
  CreateAuthor,
  deleteAuthor,
  updateAuthorById,
  uploadImage,
} from "../services/author_service";

const Category = () => {
  const [author, setAuthor] = useState([]);
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [authorId, setauthorId] = useState(null);
  const [imageUrl, setImageUrl] = useState(
    "https://socialistmodernism.com/wp-content/uploads/2017/07/placeholder-image.png"
  );
  const [imageFile, setImageFile] = useState(null);
  const [isUpdate, setIsUpdate] = useState(false);
  useEffect(() => {
    const fetch = async () => {
      let author = await fetchAuthor();
      setAuthor(author);
    };
    fetch();
  }, []);
  let emailPattern = /^\S+@\S+\.[a-z]{3}$/g;
  let emailValid = emailPattern.test(email.trim());
  const onAddAuthor = async (e) => {
    console.log(e);
    e.preventDefault();
    let author = {
      name,
      email,
    };
    if (imageFile) {
      let url = await uploadImage(imageFile);
      author.image = url;
    }
    if (emailValid) {
      CreateAuthor(author).then((message) => alert(message)).then(()=>{
        onFetch()
        onResetForm()
      })
    } else {
      alert("Email Invalid");
    }
  };
  const onDelete = (id) => {
    deleteAuthor(id)
      .then((message) => alert(message))
      .then(() => {
        let renew = author.filter((item) => item._id !== id);
        setAuthor(renew);
      });
  };

  const onUpdate = async (id, author) => {
      console.log(author.image)
    setauthorId(id);
    setName(author.name);
    setEmail(author.email);
    setImageUrl(author.image);
    setIsUpdate(true);
  };
  const onSubmitUpdate = async () => {
    let authorData = {
      name,
      email,
    };
    if (imageFile) {
      let url = await uploadImage(imageFile);
      authorData.image = url;
    }
    if (emailValid) {
      updateAuthorById(authorId, authorData).then((message) => alert(message)).then(() => {
        onFetch()
        onResetForm()
      });
      setIsUpdate(false);
    } else {
      alert("Email Invalid");
    }
  };
  const onResetForm=()=>{
      setName("")
      setEmail("")
      setImageUrl("")
  }
  const onFetch=()=>{
    const fetch = async () => {
        let author = await fetchAuthor();
        setAuthor(author);
      };
      fetch();
  }
  return (
    <Container>
      <h1 className="my-3">Author</h1>
      <Row>
        <Col md={8}>
          <Form>
            <Form.Group controlId="name">
              <Form.Label>Name</Form.Label>
              <Form.Control
                type="text"
                placeholder="Name"
                value={name}
                onChange={(e) => setName(e.target.value)}
              />
              <Form.Label className="mt-2">Email</Form.Label>
              <Form.Control
                type="text"
                placeholder="Email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
              <Button
                className="mt-3"
                variant="secondary"
                onClick={isUpdate ? onSubmitUpdate : onAddAuthor}
              >
                {isUpdate ? "Save" : "Add"}
              </Button>
              <Form.Text className="text-muted"></Form.Text>
            </Form.Group>
          </Form>
        </Col>
        <Col md={4}>
          <img
            className="w-100 border"
            id="img"
            src={
              imageUrl
                ? imageUrl
                : "https://socialistmodernism.com/wp-content/uploads/2017/07/placeholder-image.png"
            }
          />
          <Form>
            <Form.Group>
              <Form.File
                id="img"
                onChange={(e) => {
                  let url = URL.createObjectURL(e.target.files[0]);
                  setImageFile(e.target.files[0]);
                  setImageUrl(url);
                }}
              />
            </Form.Group>
          </Form>
        </Col>
      </Row>

      <Table striped bordered hover>
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>Email</th>
            <th>Image</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {author.map((author, index) => (
            <tr key={author._id}>
              <td>{index + 1}</td>
              <td>{author.name}</td>
              <td>{author.email}</td>
              <td>
                {" "}
                <img
                  width="50"
                  //   className="w-100"
                  src={
                    author.image
                      ? author.image
                      : "https://socialistmodernism.com/wp-content/uploads/2017/07/placeholder-image.png"
                  }
                />
              </td>
              <td>
                <Button
                  size="sm"
                  variant="warning"
                  onClick={() => onUpdate(author._id, author)}
                >
                  Edit
                </Button>{" "}
                <Button
                  size="sm"
                  disabled={isUpdate}
                  variant="danger"
                  onClick={() => onDelete(author._id)}
                >
                  Delete
                </Button>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </Container>
  );
};

export default Category;
