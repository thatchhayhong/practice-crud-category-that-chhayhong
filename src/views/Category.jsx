import React, { useEffect, useState } from "react";
import { Container, Form, Table, Button } from "react-bootstrap";
import { fetchCategory,postCategory,deleteCategory,updateCategoryById } from "../services/category_service";

const Category = () => {
  const [category, setcategory] = useState([]);
  const [name, setName] = useState("");
  const [categoryId,setCategoryId]=useState(null)
  useEffect(() => {
    const fetch = async () => {
      let category = await fetchCategory();
      setcategory(category);
      console.log(category);
    };
    fetch();
  }, []);
  const onAddCategory = async (e) => {
    console.log(e)
    e.preventDefault();
    let category = {
      name,
    };
    postCategory(category).then((message) => alert(message));
  };
  const onDelete =(id)=>{
    console.log(id)
    deleteCategory(id).then(message=>alert(message))
}

const onUpdate = async(id,name)=>{
  setCategoryId(id)
  setName(name)
  console.log(id)
}
const onSubmitUpdate=()=>{
  console.log(name)
  console.log(categoryId)
  let category = {
    name
}
updateCategoryById(categoryId,category).then(message=>alert(message))
}

  return (
    <Container>
      <h1 className="my-3">Category</h1>
      <Form>
        <Form.Group controlId="category">
          <Form.Label>Category Name</Form.Label>
          <Form.Control
            type="text"
            placeholder="Category Name"
            value={name}
            onChange={(e) => setName(e.target.value)}
          />
          <br />
          <Button variant="secondary" onClick={onAddCategory}>Add</Button>
          <Button variant="secondary" onClick={onSubmitUpdate}>Update</Button>
          <Form.Text className="text-muted"></Form.Text>
        </Form.Group>
      </Form>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {category.map((category, index) => (
            <tr key={category.id}>
              <td>{index + 1}</td>
              <td>{category.name}</td>
              <td>
                <Button size="sm" variant="warning" onClick={()=>onUpdate(category._id,category.name)}>
                  Edit
                </Button>{" "}
                <Button size="sm" variant="danger" onClick={()=>onDelete(category._id)}>
                  Delete
                </Button>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </Container>
  );
};

export default Category;
